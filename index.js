var log = require('node-log');
var vk = require('vk-sdk');
var async = require('async');
var _ = require('underscore');

var argv = require('minimist')(process.argv);
var config = require('./config.js');

var vk_app_id = config.vk_app_id;
var users = config.users;
var vk_scope = "wall,offline,video,pages,groups,stats,friends,messages,photos";
var m = "index";

var default_ids = null;

// обрабатываем данные которые получили при запуске скрипта
processInputParams(argv);

// вывести на экран сслыку для получения access token
// log.info(generateAuthLink(), m);

// пролайкать пост по ссылке всем ботам
// var like_url = "https://vk.com/kgtv.kremen?w=wall-69310253_2563";
// likeVkObj(like_url , function(err){
// 	if(err) return log.err(err, m);
// 	log.info("liking done");
// })

// пролайкать пост по ссылке ботам с ID 1 и 2
// var like_url = "https://vk.com/kgtv.kremen?w=wall-69310253_2563";
// likeVkObj([1, 2], like_url , function(err){
// 	if(err) return log.err(err, m);
// 	log.info("liking done");
// })

// репостнуть пост по ссылке всем ботам
// var repost_url = "https://vk.com/kgtv.kremen?w=wall-69310253_2574";
// repostVkWallPost(repost_url , function(err){
// 	if(err) return log.err(err, m);
// 	log.info("reposting done");
// })

// репостнуть пост по ссылке ботам с ID 1 и 2
// var repost_url = "https://vk.com/kgtv.kremen?w=wall-69310253_2563";
// repostVkWallPost([1, 2], repost_url , function(err){
// 	if(err) return log.err(err, m);
// 	log.info("reposting done");
// })

/*============ Input ============*/

function processInputParams(argv){
	if(argv.u || argv.users) listUsers();
	if(argv.a || argv.auth) printAuthLink();
	if(argv.c || argv.count){
		var count = getSingleValue(argv, ["c", "count"]);
		default_ids = randomUserIDs(count);
	}
	if(argv.id) default_ids = parseIdStr(argv.id);
	if(default_ids) log.info("input id: " + JSON.stringify(default_ids), m);
	var keys = Object.keys(argv);
	async.eachSeries(keys, function(key, cb){
		if(key == "l") return likeVkObj(argv[key], cb);
		if(key == "like") return likeVkObj(argv[key], cb);
		if(key == "r") return repostVkWallPost(argv[key], cb);
		if(key == "repost") return repostVkWallPost(argv[key], cb);
		return cb();
	}, function(err){
		if(err) return log.error(err, m);
		log.info("all operations done", m);
	});
}

function getSingleValue(argv, keys){
	for(var i = 0; i < keys.length; i++){
		var key = keys[i];
		if(argv[key]) return argv[key];
	}
	return null;
}

function parseIdStr(str){
	var arr = str.split(",");
	for(var i = 0; i < arr.length; i++)
		arr[i] = parseInt(arr[i].trim());
	arr = _.filter(arr, function(item){
		if(item === null) return false;
		if(isNaN(item)) return false;
		if(typeof item === "undefined") return false;
		return true;
	});
	if(arr.length == 0) return null;
	return arr;
}

function listUsers(){
	log.info("Users list", m);
	_.each(users, function(user){
		console.log(user.id + ": " + user.name);
	});
}

function printAuthLink(){
	log.info("Auth link: " + generateAuthLink(), m);
}

/*============ Functions ============*/

function likeVkObj(user_ids, url, cb){
	if(!cb && (typeof url == "function")){cb = url; url = user_ids; user_ids = default_ids ? default_ids : allUserIDs();}
	if(typeOf(url) != "array") url = [url];
	async.eachSeries(url, function(url, cb){
		async.eachSeries(user_ids, function(user_id, cb){
			likeVkObjWithUserID(user_id, url, cb);
		}, cb);
	}, cb);	
}

function likeVkObjWithUserID(id, url, cb){
	var user = userWithID(id);
	if(!user) return cb("user with id: "+id+" not found");
	var opt = parseVkObjURL(url);
	if(!opt) return cb("wrong object url");
	opt.access_token = user.token;
	log.info(user.name + ": liking obj with url: " + url, m);
	vk.likes.add(opt, cb);
}

function repostVkWallPost(user_ids, url, cb){
	if(!cb && (typeof url == "function")){cb = url; url = user_ids; user_ids = default_ids ? default_ids : allUserIDs();}
	if(typeOf(url) != "array") url = [url];
	async.eachSeries(url, function(url, cb){
		async.eachSeries(user_ids, function(user_id, cb){
			repostVkWallPostWithUserID(user_id, url, cb);
		}, cb);
	}, cb);	
}

function repostVkWallPostWithUserID(id, url, cb){
	var user = userWithID(id);
	if(!user) return cb("user with id: "+id+" not found");
	var object_id = parseVkPostURL(url);
	if(!object_id) return cb("wrong post url");
	var opt = {};
	opt.object = object_id;
	opt.access_token = user.token;
	log.info(user.name + ": reposting post with url: " + url, m);
	vk.wall.repost(opt, cb);
}

function parseVkPostURL(url){
	var regex = /wall[\d-]+_\d+/g;
	var match = regex.exec(url);
	if(match.length == 0) return null;
	return match[0];
}

function parseVkObjURL(url){
	var regex = /(wall|post|comment|photo|audio|video|note|photo_comment|video_comment|topic_comment|sitepage)([\d-]+)_(\d+)/g;
	var match = regex.exec(url);
	if(match.length == 0) return null;
	var data = {};
	data.type = match[1];
	data.owner_id = match[2];
	data.item_id = match[3];
	if(data.type == "wall") data.type = "post";
	return data;
}

function generateAuthLink(){
	return vk.clientAuthLink(vk_app_id, vk_scope);
}

function userWithID(user_id){
	return _.find(users, function(user){return user.id == user_id});
}

function allUserIDs(){
	return _.map(users, function(user){return user.id});
}

function randomUserIDs(count){
	count = parseInt(count);
	if(!count) count = users.length;
	if(isNaN(count)) count = users.length;
	var ids = allUserIDs();
	return _.sample(ids, count);
}

function typeOf(value) {
    var s = typeof value;
    if (s === 'object') {
        if (value) {if (Object.prototype.toString.call(value) == '[object Array]') s = 'array';}
        else {s = 'null';}
    }
    return s;
}